import React from 'react'
import Select from 'react-select'
import './style.sass'

const options = [
  { value: '', label: '', },
  { value: '', label: '', },
  { value: '', label: '' }
]

const customStyles = {
  indicatorSeparator: () => ({
    display: 'none'
  }),

  menu: () => ({
    position: 'absolute',
    width: 880,
    margin: 'auto',
    background: 'white',
    border: '1px solid #bfbfbf',
    zIndex: 1,
    left: 32,
    top: 50,
  }),
  
  indicatorsContainer: () => ({
    width: 14,
    height: 7,
    marginRight: 25,
    
  }), 

  control: () => ({
    // none of react-select's styles are passed to <Control />
    position: 'relative',
    width: 886,
    height: 44,
    padding: 0,    
    margin: 'auto',
    marginBottom: 16,
    backgroundColor: '#F5F5F5',
    borderBottom: '1px solid #E0E0E7',
    display: 'flex',
    alignItems: 'center',
    fontFamily: 'IBM Plex Mono',
    fontSize: 13,
    lineHeight: '150%',
    textIndent: 16
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition };
  }
}

const SelectComponent = ({placeholder}) => (
  <Select 
    styles={customStyles}
    options={options}
    placeholder={placeholder}
  />
)

export default SelectComponent;



