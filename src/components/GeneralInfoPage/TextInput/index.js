import React from 'react'
import './index.css'
// import { Formik } from "formik";

// const TextInput = () => (
//      <Formik
//        initialValues={{ email: '', password: '' }}
//        validate={values => {
//          const errors = {};
//          if (!values.email) {
//            errors.email = 'Required';
//          } else if (
//            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
//          ) {
//            errors.email = 'Invalid email address';
//          }
//          return errors;
//        }}
//        onSubmit={(values, { setSubmitting }) => {
//          setTimeout(() => {
//            alert(JSON.stringify(values, null, 2));
//            setSubmitting(false);
//          }, 400);
//        }}
//      >
//        {({
//          values,
//          errors,
//          touched,
//          handleChange,
//          handleBlur,
//          handleSubmit,
//          isSubmitting,
//          /* and other goodies */
//        }) => (
//          <form onSubmit={handleSubmit} className="input_container">
//            <input
//              type="email"
//              name="email"
//              onChange={handleChange}
//              onBlur={handleBlur}
//              value={values.email}
//            />
//            {errors.email && touched.email && errors.email}
//            <input
//              type="password"
//              name="password"
//              onChange={handleChange}
//              onBlur={handleBlur}
//              value={values.password}
//            />
//            {errors.password && touched.password && errors.password}
//            <button type="submit" disabled={isSubmitting}>
//              Submit
//            </button>
//          </form>
//        )}
//      </Formik>
// )

const TextInput = ({
    name,
    title,
    type,
    value,
    onChange,
    onBlur,
    errors,
    touched,
    required
}) => (
    <div>
    <div className={required ? "input_wrapper required" : "input_wrapper"}>
        <div className={(errors && touched) ? "input_container error_input" : "input_container"}>
            <input 
                className='text_input'
                type={type}
                name={name}
                value={value}
                placeholder={title}
                onChange={onChange}
                onBlur={onBlur}/>
        </div>
        
    </div>
    {(errors && touched) && <span className="error_message">{errors}</span>}
    </div>
)

export default TextInput
