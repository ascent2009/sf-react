import React from 'react';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FastField} from 'formik';
import * as Yup from 'yup';
import './style.sass';

const initialValues = {
  firstName: '',
  lastName: '',
  city: '',
  dateOfBirth: '',
  telegram: '',
  phone: '',
  email: '',
  social: {
    facebook: '',
    vk: '',
    linkedIn: '',
    instagram: '',
  },
}

const onSubmit = e => {
  e.preventDefault();
}

const validationSchema = Yup.object({
    firstName: Yup.string().required('Обязательное поле *'),
    lastName: Yup.string().required('Обязательное поле *'),
    city: Yup.string(),
    dateOfBirth: Yup.date().required('Обязательное поле *'),
    phone: Yup.string().required('Обязательное поле *'),
    email: Yup.string().email('Invalid email format').required('Обязательное поле *'),
})

const FormComponent = () => {
  let html = document.createElement('span');
  html.append('+7');
  // html.classList.add('.span');
  console.log(html.innerHTML);
  const phonePlaceholder = {
    code: html.innerHTML,
    number: '(999)999-99-99'
  }

  
  return (
    <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
    >
        <Form className="formBlock">
            <div className="inputBlock">
                {/* <label htmlFor="firstName" className="label" /> */}
                <Field
                  type="text"
                  className="input"
                  id="firstName"
                  name="firstName"
                  placeholder="Имя"
                   />
                  <ErrorMessage name='firstName'>
                    {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
                  </ErrorMessage>
            </div>
            <div className="inputBlock">
                {/* <label htmlFor="lastName" className="label" /> */}
                <Field
                  type="text"
                  className="input"
                  id="lastName"
                  name="lastName"
                  placeholder="Фамилия" />
                  <ErrorMessage name='lastName'>
                    {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
                  </ErrorMessage>
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="city" className="label" /> */}
              <FastField name="city">
                {props => {
                  const {field, meta} = props;
                  return (
                  <div>
                    <input type="text" className="input" id="city" placeholder="Город" { ... field} />
                    {meta.touched && meta.error ? <div>{meta.error}</div> : null}
                  </div>)
                }}
              </FastField>
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="dateOfBirth" className="label" /> */}
              <Field
                type="date"
                className='input'
                id="dateOfBirth"
                name="dateOfBirth"
                placeholder="Дата рождения"
              />
              <ErrorMessage name='dateOfBirth'>
                {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
              </ErrorMessage>
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="phone" className="label" /> */}
              <Field
                type="text"
                className="input"
                id="phone"
                name="phone"
                placeholder={phonePlaceholder.code + phonePlaceholder.number}
                />
                <ErrorMessage name='phone'>
                  {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
                </ErrorMessage>
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="telegram" className="label" /> */}
              <Field type="text" id="telegram" className="input" name="telegram" placeholder="Telegram"/>
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="email" className="label" /> */}
              <Field
                type="email"
                className="input"
                id="email"
                name="email"
                placeholder="Почта" />
              <ErrorMessage name='email'>
                {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
              </ErrorMessage>
            </div>
            
            <div className="inputBlock">
              {/* <label htmlFor="facebook" className="label" /> */}
              <Field type="text" id="facebook" name="social.facebook" className="input" placeholder="Facebook" />
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="vk" className="label" /> */}
              <Field type="text" id="vk" name="social.vk" className="input" placeholder="Vk" />
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="linkedIn" className="label" /> */}
              <Field type="text" id="linkedIn" name="social.linkedIn" className="input" placeholder="LinkedIn" />
            </div>
            <div className="inputBlock">
              {/* <label htmlFor="instagram" className="label" /> */}
              <Field type="text" id="instagram" name="social.instagram" className="input" placeholder="Instagram" />
            </div>

            {/* <button type="submit" className="btn">Сохранить</button> */}
        </Form>
    </Formik>
)} 

export default FormComponent;