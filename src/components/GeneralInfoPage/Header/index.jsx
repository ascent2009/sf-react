import './style.sass';
import LogoSvg from '../assets/logo.svg';
import BellSvg from '../assets/bell.svg';
import { headerMenu, SFURL } from '../routes.js';

const Header = () => {
    const user = 'User name';
    return (
        <div className="headerBlock">
            <div className="headerContent">
                <a href={SFURL}><img src={LogoSvg} alt="Лого"/></a>
                <nav className="headerMenu">
                    {headerMenu.map((item, i) => {
                        return (
                            <a href="/" key={i}>{item}</a>
                        )
                    })}
                </nav>
                <div className="imgBlock">
                    <img src={BellSvg} alt="Звонок" />
                    <p>{user}</p>
                </div>
                
            </div>
        </div>
    ) 
}

export default Header;