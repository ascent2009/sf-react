import React, {useState, useRef} from 'react'
import './style.sass'
import { keySkills, portfolioLinks } from '../routes'
import CloseSvg from '../assets/close.svg'


const AddInputValue = (props) => {
    const [item, setItem] = useState('');
    const [selected, setSelected] = useState(['CustDev', 'CustDev', 'CustDev']);
    const [url, setUrl] = useState([...portfolioLinks])
    const [logged, setLogged] = useState(false)
    
    const handleLoggedElement = () => {
        setLogged(true);
        setLogged(false);
    }
    
    const inputItem = (e) =>{
        const value = e.target.value;
        setItem(value);
    }

    const addItem = () => {
        if (!item) return;
        setSelected([...selected, item]);
        console.log(logged);
        setItem('');
    }

    const addURL = () => {
        if (!item) return;
        setUrl([...url, item]);
        // console.log(logged);
        setItem('');    
    }

    const addItemFromSkills = (e) => {
        const name = e.target.dataset.name
        setSelected([...selected, name])
    }

    const deleteItem = (e) => {
        const idx = e.target.dataset.id
        const newArr = [...selected.slice(0, idx), ...selected.slice(idx + 1)];
        setSelected(newArr)
    }
    
    const deleteUrl = (e) => {
        // const idx = selected.findIndex((elem) => elem.id === id);
        const idx = e.target.dataset.id
        const newArr = [...url.slice(0, idx), ...url.slice(idx + 1)];
        console.log('idx: ', idx, e.target.dataset.id);
        setUrl(newArr)
    }

        const myRef = useRef(null);
        const node = myRef.current;
        console.log('node: ', node);

    return (
        <>
            <ul className="selectedItems" style={props.selected} ref={myRef} onLoad={handleLoggedElement}>
                {selected.map((it, index) => (
                    <li className="selectedItem" key={index}>
                        {it}
                        <img src={CloseSvg} data-id={index} alt="удалить" onClick={deleteItem}/>
                    </li>
                ))}
            </ul>
            <form className="formValue" style={props.input}> 
                <input type="text" name="text" className="inputText" onChange={(e) => inputItem(e)} placeholder={props.placeholder}/>
                <button
                    type="button"
                    className="addButton"
                    onClick={
                        logged ?
                        addItem : addURL
                    }
                >Добавить
                </button>
                <div className="dropdownModal" style={props.selected}>
                    <h3>Рекоммендуемые компетенции</h3>
                    <ul className="listValue">
                        {keySkills.map((item, index) => (
                            <li className="itemListValue" key={index} data-name={item} onClick={(e) => addItemFromSkills(e)}>{item}</li>
                        ))}
                    </ul>
                </div>
            </form>
            <ul className="portfolioList" style={props.portfolio}>
                {url.map((link, index) => (
                    <li className="portfolioURL" key={index}>
                        <a href={`https://${link}`} target="_blank" rel="noreferrer">{link}</a>
                        <img src={CloseSvg} data-id={index} alt="удалить" onClick={deleteUrl}/>
                    </li>
                ))}
            </ul>
        </>
    );
}

export default AddInputValue;