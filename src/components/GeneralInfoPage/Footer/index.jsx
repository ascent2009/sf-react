import './style.sass';
import LogoSvg from '../assets/logoshort.svg';
import EnvelopeSvg from '../assets/envelope.svg';
import {contacts, socialNetork, SFURL} from '../routes.js';

const Footer = () => {
    
    return (
        <div className="footerBlock">
            <div className="footerContent">
                <div className="footerUpper">
                    <a href={SFURL}><img src={LogoSvg} alt="Лого" /></a>
                    <div className="blockWrapper">
                        <div className="block">
                            <p className="label">{contacts.label}</p>
                            <div className="itemName">
                                <img src={EnvelopeSvg} alt="Конверт" />
                                <a href={`mailto: ${contacts.email}`}>{contacts.email}</a>
                            </div>
                        </div>
                        <div className="block">
                            <p className="label">{socialNetork.label}</p>
                            <div className="itemName">
                                {socialNetork.social.map((item) => {
                                    return (
                                        <a href={item.url} key={item.name} target="_blank" rel="noreferrer noopener">
                                        {item.name}
                                        </a>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footerLower">
                    <div className="legalBlock">
                        <a href="/">Политика конфиденциальности</a>
                        <a href="/">ИНН</a>
                        <a href="/">Организация</a>
                    </div>
                </div>
            </div>
        </div>
    ) 
}

export default Footer;