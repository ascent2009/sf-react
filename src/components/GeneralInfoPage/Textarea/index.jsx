import React from 'react';
import './style.sass'
import { btnSvg } from '../routes'

const TextArea = (props) => (
    <div className="aboutBlock">
        <textarea
            className="aboutMe"
            name="about"
            id="about"
            placeholder={props.placeholder}
        />
        <div className="editPanel">
            {btnSvg.map((item) => 
                <button className="formatTextButton">
                    <img src={item.src} alt={item.alt} title={item.title} key={item.alt} />
                </button>
            )}
        </div>
    </div>
);


export default TextArea;