import './style.sass';
import Header from './Header';
import MainPage from './Main';
import Footer from './Footer';

const GeneralInfoPage = () => {
    return (
        <>
            <Header />
            <MainPage />
            <Footer />
        </>
    ) 
}

export default GeneralInfoPage;