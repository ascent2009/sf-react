import { useState } from 'react'
import { sideBar, placeholders, inputStyles } from '../routes';
import FormComponent from '../FormComponent';
import SelectComponent from '../SelectComponent'
import TextArea from '../Textarea'
import QuestionSvg from '../assets/question.svg'
import AddInputValue from '../AddInputValue'
import Title from '../Title'
import './style.sass';

const MainPage = () => {

    const active = 'str';
    const options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        };
    const saveProfileDate = new Date().toLocaleString('ru', options).replace(',', '');

    const [modal, setModal] = useState('');
    
    const displayPopup = () => {
        // const popup = sideBar.map((elem) => elem.id).join(" ").indexOf('langs');
        setModal('popup');
        // console.log(modal);
    }

    const closePopup = () => {
        setModal('');
        console.log(modal);
    }
    
    return (
        <>
            <div className="mainBlock">
                <a href="/"><div className="toEditMode">Назад к редактрованию</div></a>
                <div className="mainContent">
                    <aside className="sideBar">
                        <ul className="sideBarList">
                            {sideBar.map(item => 
                                <li><a href={`#${item.id}`}>{item.name}</a></li>
                            )}
                        </ul>
                        <div className="toolsBlock">
                            <button type="button">Сохранить</button>
                            <button type="button">Предпросмотр</button>
                            <div className="switchBlock">
                                <label className="switch" htmlFor="check">
                                    <input type="checkbox" id="check"/>
                                    <span className="slider"></span>
                                </label>
                                <p>Опубликовать</p>
                            </div>
                        </div>       
                    </aside>
                    <section className="summaryBlock">
                        <article id="general">
                            <div className="titleBlock">
                                <h2 className="title">Основная информация</h2>
                                <p className="dateText">Профиль обновлён: {saveProfileDate}</p>
                            </div>
                            <div className="personalBlock">
                                <div className="photo" />
                                <FormComponent />
                            </div>
                        </article>
                        <article id="proffesion">
                            <Title 
                                title={sideBar[1].name}
                            />
                            <SelectComponent placeholder={placeholders.select[0]} />
                            <SelectComponent placeholder={placeholders.select[1]} />
                        </article>
                        <article id="about">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[2].name}
                                id={sideBar[2].id}
                            />
                            <TextArea placeholder={placeholders.textarea[0]} />
                        </article>
                        <article id="keyskills">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                active={active}
                                title={sideBar[3].name}
                                id={sideBar[3].id}
                            />
                            <AddInputValue 
                                placeholder={placeholders.input[0]}
                                portfolio={inputStyles.portfolio}
                            />
                        </article>
                        <article id="experience">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[4].name}
                                id={sideBar[4].id}
                            />
                            <AddInputValue
                                input={inputStyles.input}
                                portfolio={inputStyles.portfolio}
                            />
                            <SelectComponent placeholder={placeholders.select[2]} />
                            <TextArea placeholder={placeholders.textarea[1]} />
                        </article>
                        <article id="softskills">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[5].name}
                                id={sideBar[5].id}
                            />
                            <TextArea placeholder={placeholders.textarea[2]} />
                        </article>
                        <article id="portfolio">
                            <Title 
                                title={sideBar[6].name}
                            />
                            <AddInputValue 
                                selected={inputStyles.selected}
                                placeholder={placeholders.input[1]}
                            />
                        </article>
                        <article id="education">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[7].name}
                                id={sideBar[7].id}
                            />
                            <TextArea placeholder={placeholders.textarea[3]} />
                        </article>
                        <article id="extra-education">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[8].name}
                                id={sideBar[8].id}
                            />
                            <TextArea placeholder={placeholders.textarea[4]} />
                        </article>
                        <article id="langs">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[9].name}
                                id={sideBar[9].id}
                            />
                            <TextArea placeholder={placeholders.textarea[5]} />
                        </article>
                        <article id="extra-info">
                            <Title 
                                displayPopup={displayPopup}
                                closePopup={closePopup}
                                svg={QuestionSvg}
                                modal={modal}
                                title={sideBar[10].name}
                                id={sideBar[10].id}
                            />
                            <TextArea placeholder={placeholders.textarea[6]} />
                        </article>
                        <article id="internship">
                            <Title 
                                title={sideBar[11].name}
                            />
                            <SelectComponent placeholder={placeholders.select[3]} />
                        </article>
                        <article id="terms">
                            <Title 
                                title={sideBar[12].name}
                            />
                            <SelectComponent placeholder={placeholders.select[4]} />
                        </article>
                        <article id="search-status">
                            <Title 
                                title={sideBar[13].name}
                            />
                            <SelectComponent placeholder={placeholders.select[5]} />
                        </article>
                    </section>
                </div>
            </div>
        </>
    )
}

export default MainPage;