import React from 'react';
import Popup from '../Popup'

const Title = (props) => {
    return (
    <div className="titleBlock" style={{justifyContent: 'left'}}>
        <h2 className="title">{props.title}</h2>
        {props.svg
        ? <div className="titleImg"
            // onMouseOver={props.displayPopup}
            // onMouseLeave={props.closePopup}
        >
            <img src={props.svg} alt="Подробнее" />
            {/* {props.modal ? */}
             <Popup text={props.title}/> 
             {/* : null} */}
        </div>
        : null}
    </div>)
}

export default Title;