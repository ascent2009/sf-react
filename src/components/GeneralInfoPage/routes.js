import BoldSvg from "./assets/bold.svg";
import ItalicSvg from "./assets/italic.svg";
import UnderlineSvg from "./assets/underline.svg";
import HamburgerSvg from "./assets/hamburger.svg";
import CancelSvg from "./assets/cancel.svg";

// URLs

export const SFURL = "http://www.skillfactory.ru";
export const portfolioLinks = [
  "https://www.behance.net/andrei-1901895",
  "https://www.behance.net/andrei-1901895",
];

// текст по умолчанию в полях компонентов

export const placeholders = {
  select: [
    "Выбери профессию",
    "Выбери уровень в профессии",
    "Отрасль",
    "Мне не нужна стажировка",
    "Удалённо",
    "Ищу работу",
  ],
  textarea: [
    "Напиши немного о себе",
    "Напиши о своем опыте работы",
    "Опиши свои софт скиллы",
    "Напиши о своем образовании",
    "Напиши о своем дополнительном образовании",
    "Напиши о знаниях иностранных языков",
    "Напиши дополнительно о себе",
  ],
  input: ["Заполни ключевые компетенции", "Укажи ссылку на портфолио"],
};

// Иконки кнопок редактирования текста в компоненте Textarea

export const btnSvg = [
  {
    src: BoldSvg,
    alt: "Полужирный текст",
    title: "Полужирный текст",
    action: {
      fontWeight: "bold",
      //   fontStyle: "normal",
      //   textDecoration: "none",
    },
  },
  {
    src: ItalicSvg,
    alt: "Курсив",
    title: "Курсив",
    action: {
      //   fontWeight: "normal",
      fontStyle: "italic",
      //   textDecoration: "none",
    },
  },
  {
    src: UnderlineSvg,
    alt: "Подчеркивание",
    title: "Подчеркивание",
    action: {
      //   fontWeight: "normal",
      //   fontStyle: "normal",
      textDecoration: "underline",
    },
  },
  {
    src: HamburgerSvg,
    alt: "Абзац",
    title: "Абзац",
    action: {
      //   fontWeight: "normal",
      //   fontStyle: "normal",
      //   textDecoration: "underline",
      listStyleType: "circle",
    },
  },
  {
    src: CancelSvg,
    alt: "Отменить",
    title: "Отменить",
    action: {
      fontWeight: "normal",
      fontStyle: "normal",
      textDecoration: "none",
    },
  },
  {
    src: CancelSvg,
    alt: "Вернуть",
    title: "Вернуть",
  },
];

// Элементы меню шапки

export const headerMenu = ["Профиль", "Компании", "Вакансии", "Мои отклики"];

// Элементы меню футера

export const contacts = {
  label: "Контакты:",
  email: "email@example.ru",
};

export const socialNetork = {
  label: "Поделиться в соц.сетях",
  social: [
    {
      name: "ВКонтакте",
      url: "https://vk.com/skillfactoryschool",
    },
    {
      name: "Facebook",
      url: "https://www.facebook.com/skillfactoryschool/",
    },
    {
      name: "Instagram",
      url: "https://www.instagram.com/skillfactoryschool/",
    },
    {
      name: "LinkedIn",
      url: "https://www.linkedin.com/company/skillfactory-school/",
    },
  ],
};

// элементы сайдбара основного блока

export const sideBar = [
  {
    name: "Основная информация",
    id: "general",
  },
  {
    name: "Профессия",
    id: "profession",
  },
  {
    name: "Обо мне",
    id: "about",
  },
  {
    name: "Ключевые компетенции",
    id: "keyskills",
  },
  {
    name: "Опыт работы",
    id: "experience",
  },
  {
    name: "Soft Skills",
    id: "softskills",
  },
  {
    name: "Портфолио",
    id: "portfolio",
  },
  {
    name: "Образование",
    id: "education",
  },
  {
    name: "Дополнительное образование",
    id: "extra-education",
  },
  {
    name: "Иностранные языки",
    id: "langs",
  },
  {
    name: "Дополнительная информация",
    id: "extra-info",
  },
  {
    name: "Стажировка",
    id: "internship",
  },
  {
    name: "Условия работы",
    id: "terms",
  },
  {
    name: "Статус поиcка работы",
    id: "search-status",
  },
];

// Элементы списка ключевых компетенций

export const keySkills = ["CustDev", "CJM", "User Story Map", "User Persona"];

// Стили элементов компонента AddInputValue

export const inputStyles = {
  selected: { display: "none" },
  input: { display: "none" },
  portfolio: { display: "none" },
};
