import React, { useState } from "react";
import "./style.sass";
import { btnSvg } from "../../GeneralInfoPage/routes";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const TextArea = (props) => {
  const { value, key, name, onChange } = props;

  const [edit, setEdit] = useState({});

  const initialValues = {
    personalInfo: value,
    education: value,
  };

  console.log(initialValues);

  const onSubmit = (e) => {
    e.preventDefault();
  };

  const validationSchema = Yup.object({
    personalInfo: Yup.string().required("Поле обязательно для заполнения"),
    education: Yup.string().required("Поле обязательно для заполнения"),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div className="aboutBlock">
          <Field
            type="text"
            as="textarea"
            className="aboutMe"
            id="personalInfo"
            name={name}
            placeholder={props.placeholder}
            style={edit}
            value={value}
            onChange={onChange}
          />
          {!value ? (
            <ErrorMessage name={name}>
              {(ErrorMessage) => <div className="error">{ErrorMessage}</div>}
            </ErrorMessage>
          ) : null}

          <div className="editPanel">
            {btnSvg.map((item, id) => (
              <button
                key={id}
                className="formatTextButton"
                onClick={() => setEdit(item.action)}
              >
                <img
                  src={item.src}
                  alt={item.alt}
                  title={item.title}
                  key={item.alt}
                />
              </button>
            ))}
          </div>
        </div>
      </Form>
    </Formik>
  );
};

export default TextArea;
