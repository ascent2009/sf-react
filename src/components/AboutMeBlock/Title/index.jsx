import React, { useState } from "react";
import Popup from "../Popup";

const Title = (props, { children }) => {
  const { title, svg, hint } = props;
  const [open, setOpen] = useState(false);

  return (
    <div className="titleBlock" style={{ justifyContent: "left" }}>
      {children}

      <h2 className="title">
        <span style={{ color: "red", fontSize: "24px" }}>*&nbsp;</span>
        {title}
      </h2>
      {svg ? (
        <div
          className="titleImg"
          onMouseOver={() => setOpen(true)}
          onMouseLeave={() => setOpen(false)}
        >
          <img src={svg} alt="Подробнее" />
          <Popup text={!hint ? title : hint} isOpen={open} />
        </div>
      ) : null}
    </div>
  );
};

export default Title;
