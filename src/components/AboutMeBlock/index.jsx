import "../GeneralInfoPage/style.sass";
import Header from "../GeneralInfoPage/Header";
import MainPage from "./Main";
import Footer from "../GeneralInfoPage/Footer";

const AboutMeBlock = () => {
  return (
    <>
      <Header />
      <MainPage />
      <Footer />
    </>
  );
};

export default AboutMeBlock;
