import React from "react";
import "./style.sass";

function Popup({ isOpen, text }) {
  // const lower = text.toLowerCase();
  if (!isOpen) return null;
  return (
    <div className="popup">
      <p>{text}</p>
    </div>
  );
}

export default Popup;
