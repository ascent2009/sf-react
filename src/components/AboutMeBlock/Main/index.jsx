import { useState, useCallback } from "react";
import { placeholders, sideBar } from "../../GeneralInfoPage/routes";
import TextArea from "../Textarea";
import DraftEditor from "../../DraftEditor/DraftEditor";
import QuestionSvg from "../../GeneralInfoPage/assets/question.svg";
import Title from "../Title";
import "./style.sass";

const MainPage = (initialValues = {}) => {
  const [values, setValues] = useState(initialValues);

  const onInputChange = useCallback(
    ({ target: { name, value } }) =>
      setValues((state) => ({ ...state, [name]: value })),
    []
  );

  function postData() {
    const url = "www.example.com";
    fetch(url, {
      method: "POST",
      body: JSON.stringify({ ...values }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    console.log(
      {
        ...values,
      },
      JSON.stringify(values)
    );
  }

  return (
    <>
      <div className="mainBlock">
        <div className="mainContent">
          <aside className="sideBar" style={{ background: "transparent" }}>
            <div className="toolsBlock">
              <button type="button" onClick={postData}>
                Сохранить
              </button>
            </div>
          </aside>
          <section className="summaryBlock">
            <article id="about">
              <Title
                svg={QuestionSvg}
                title={sideBar[2].name}
                id={sideBar[2].id}
                name={sideBar[2].id}
                hint="Расскажите о себе"
              />
              {/* <TextArea
                placeholder={placeholders.textarea[0]}
                name="personalInfo"
                onChange={onInputChange}
                value={values.personalInfo}
              /> */}
              <DraftEditor placeholder={placeholders.textarea[0]}
              name="personalInfo"
                onChange={onInputChange}
                value={values.personalInfo}/>
            </article>

            <article id="education">
              <Title
                svg={QuestionSvg}
                title={sideBar[7].name}
                id={sideBar[7].id}
                name={sideBar[7].id}
                hint="Укажите участие в Олимпиадах, в т.ч. школьных, хакатонах"
              />
              {/* <TextArea
                placeholder={placeholders.textarea[3]}
                name="education"
                value={values.education}
                onChange={onInputChange}
              /> */}
              <DraftEditor placeholder={placeholders.textarea[3]}
              name="education"
                value={values.education}
                onChange={onInputChange}/>
            </article>
          </section>
        </div>
      </div>
    </>
  );
};

export default MainPage;
