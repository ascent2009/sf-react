import './style.sass';

const LoadFileButton = (props) => {

    function printPage() {
        window.print();
    }

    return (
        <div className="input-file-block" >
            <input type="button" id="input-file" name="file" className="input-file" />
            <label htmlFor="input-file" className="input-img-btn"
                //  onClick={printPage}
                onClick={props.toPdf}
            >
                Скачать резюме
            </label>
        </div>)
}

export default LoadFileButton;