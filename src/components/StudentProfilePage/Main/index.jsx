import React from 'react';
import LoadFileButton from '../LoadFileButton'
import Title from '../../GeneralInfoPage/Title'
import ArticleContent from '../ArticleContent'
import AvatarSvg from '../assets/useravatar.svg'
import ScrollTopSvg from '../assets/uparrow.svg'
import VKSvg from '../assets/VKicon.svg'
import FBSvg from '../assets/FBicon.svg'
import { sideProfile, skills } from '../routes'
import { sideBar, portfolioLinks } from '../../GeneralInfoPage/routes'
import './style.sass';
import { jsPDF } from "jspdf"
import html2canvas from "html2canvas";

class MainPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: 'Андрей Снигирёв',
            avatar: AvatarSvg
        }
        this.pdfRef = React.createRef();
    }

    jsPdfGenerator = () => {
        html2canvas(this.pdfRef.current, {
            windowWidth: this.pdfRef.current.scrollWidth,
            windowHeight: this.pdfRef.current.scrollHeight})
            .then((canvas) => {
            const imgData = canvas.toDataURL("image/png");
            const pdf = new jsPDF();
            pdf.addImage(imgData, "JPEG", 0, 0, 200, 400);
            pdf.save("download.pdf");    
        }
    )}

    render() {
        const { user, avatar } = this.state;
        return (
            <div className="mainBlock">
                <a className="back_to_edit-link" href="/" rel="noreferrer noopener">Назад к редактрованию</a>
                <div className="mainContent" ref={this.pdfRef}>
                    <aside className="sideBar">
                        <img className="sideBarUserAvatar" src={avatar} alt="Фото" />
                        <h3 className="sideBarUserName">{user}</h3>
                        <ul className="sideBarList">
                            {sideProfile.map((item, i) =>
                                <li key={i}>
                                    <p className="itemLabel">{item.label}</p>
                                    <p className="itemValue">{item.value}</p>
                                </li>
                            )}
                        </ul>
                        <div className="userSocialLinks">
                            <a href="https://www.vk.com"><img src={VKSvg} alt="VK" /></a>
                            <a href="https://www.facebook.com"><img src={FBSvg} alt="FB" /></a>
                        </div>
                        <LoadFileButton toPdf={this.jsPdfGenerator} />
                    </aside>
                    <section className="summaryBlock">
                        <article>
                            <Title
                                title='Junior Product Manager'
                            />
                            <p className="textBlock">
                                Опыт управления продуктами (B2B2C & B2B) в компаниях А, Б, В.
                                Вижу, как увеличить ценность продукта.
                                Соединяю потребности пользователей с возможностями разработки.
                                Умею работать с командами и приносить результат в динамичной среде…
                            </p>
                        </article>
                        <article id="keyskills">
                            <Title
                                title="Ключевые навыки"
                            />
                            <ArticleContent
                                skills={skills}
                            />

                        </article>
                        <article id="experience">
                            <Title
                                title={sideBar[4].name}
                            />
                            <p className="textBlock">
                                <span>Крупная западная компания – лидер фармацевтической отрасли. Численность 5000 человек.
                                <br></br>
                                Зона ответственности:
                                <br></br><br></br>
                                Вся Россия, Подчинение Директору по маркетингу / Директору продуктов
                                <br></br>
                                Ответственность за создание, развитие и выпуск на рынок нескольких новых продуктов, которые привели к возврату инвестиций на 30%
                                <br></br><br></br>
                                Результаты:
                                <br></br><br></br>
                                - Запустил продукт “Калькулятор торговых условий” для переговоров, что позволило компании выиграть 5 тендеров
                                <br></br>
                                - Вывел на рынок 6 новых продукта, которые увеличили долю на рынке на 15% в категории аналогичных товаров
                                <br></br>
                                - Увеличил покрытие рынка контрактами с сетями с 60% до 72%
                                <br></br>
                                - Увеличил представленность ключевых брендов в аптеках на 47%
                                <br></br>
                                - Написал стратегию развития отдела (в том числе разработал концепции и направление развития новых продуктов) на 2019, которые успешно реализовал на 97%
                                <br></br>
                                - Внедрил продукт “Календарь Промо-активностей”, который позволил отделу продаж увеличить доходность на 15%
                                <br></br><br></br>
                                Медицинский представитель
                                <br></br>
                                Санофи Авентис - г. Москва, Июнь 2017 – Август 2020
                                <br></br>
                                Крупная западная компания – лидер фармацевтической отрасли. Численность 5000 человек.
                                <br></br><br></br>
                                Зона ответственности:
                                <br></br>
                                вся Россия, Подчинение Директору по маркетингу / Директору продуктов
                                <br></br>
                                Ответственность за создание, развитие и выпуск на рынок нескольких новых продуктов, которые привели к возврату инвестиций на 30%
                                <br></br><br></br>
                                Результаты:
                                <br></br>
                                - Запустил продукт “Калькулятор торговых условий” для переговоров, что позволило компании выиграть 5 тендеров
                                <br></br>
                                - Вывел на рынок 6 новых продукта, которые увеличили долю на рынке на 15% в категории аналогичных товаров
                                <br></br>
                                - Увеличил покрытие рынка контрактами с сетями с 60% до 72%
                                <br></br>
                                - Увеличил представленность ключевых брендов в аптеках на 47%
                                <br></br>
                                - Написал стратегию развития отдела (в том числе разработал концепции и направление развития новых продуктов) на 2019, которые успешно реализовал на 97%
                                <br></br>
                                - Внедрил продукт “Календарь Промо-активностей”, который позволил отделу продаж увеличить доходность на 15%</span>
                            </p>
                        </article>
                        <article id="softskills">
                            <Title
                                title={sideBar[5].name}
                            />
                            <p className="textBlock">
                                Опыт управления продуктами (B2B2C & B2B) в компаниях А, Б, В.
                                Вижу, как увеличить ценность продукта.
                                Соединяю потребности пользователей с возможностями разработки.
                                Умею работать с командами и приносить результат в динамичной среде…
                            </p>
                        </article>
                        <article id="portfolio">
                            <Title
                                title={sideBar[6].name}
                            />
                            <ArticleContent
                                portfolio={portfolioLinks}
                            />
                        </article>
                        <article id="education">
                            <Title
                                title={sideBar[7].name}
                            />
                            <p className="textBlock"><span>
                                Уральский Федеральный Университет - Екатеринбург – 2009
                                <br></br>
                                Факультет: Экономический
                                <br></br>
                                Специальность: Экономист</span>
                            </p>
                        </article>
                        <article id="extra-education">
                            <Title
                                title={sideBar[8].name}
                            />
                            <p className="textBlock"><span>
                                SkillFactory, 2021
                                <br></br>
                                Курс Product Management, 2021
                                <br></br><br></br>
                                Stepic
                                <br></br>
                                Курс Программирование на Python, 2020
                                <br></br><br></br>
                                Stepic
                                <br></br>
                                Курс Интерактивный тренажер по SQL, 2020</span>
                            </p>
                        </article>
                        <article id="langs">
                            <Title
                                title={sideBar[9].name}
                            />
                            <p className="textBlock"><span>
                                Английский - В2
                            <br></br>
                            Немецкий - А2</span>
                            </p>
                        </article>
                        <article id="extra-info">
                            <Title
                                title={sideBar[10].name}
                            />
                            <p className="textBlock"><span>
                                Английский - В2
                                <br></br>
                                Немецкий - А2</span>
                            </p>
                        </article>
                    </section>
                    <aside className="sideBar">
                        <button className="scroll_to_top-block" onClick={() => window.scrollTo(0, 0)}>
                            <img src={ScrollTopSvg} alt="К началу страницы" />
                            Наверх
                        </button>
                    </aside>
                </div>
            </div>
        )
    }
}

export default MainPage;