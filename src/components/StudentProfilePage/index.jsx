import '../GeneralInfoPage/style.sass';
import Header from '../GeneralInfoPage/Header';
import MainPage from './Main';
import Footer from '../GeneralInfoPage/Footer';

const StudentProfilePage = () => {
    return (
        <>
            <Header />
            <MainPage />
            <Footer />
        </>
    ) 
}

export default StudentProfilePage;