export const sideProfile = [
    {
        label: 'МЕСТОПОЛОЖЕНИЕ',
        value: 'г. Пермь'
    },
    {
        label: 'ВОЗРАСТ',
        value: '21 год'
    },
    {
        label: 'ТЕЛЕФОН',
        value: '8 (902) 839-1121'
    },
    {
        label: 'E-MAIL',
        value: 'andrei-1902va@mail.ru'
    },
    {
        label: 'СТАЖИРОВКА',
        value: 'Мне не нужна стажировка'
    },
    {
        label: 'УСЛОВИЯ РАБОТЫ',
        value: 'Удалённо'
    },
    {
        label: 'СТАТУС ПОИСКА РАБОТЫ',
        value: 'Ищу работу'
    },
    {
        label: 'СОЦИАЛЬНЫЕ СЕТИ',
        value: ''
    }
]

export const skills = [
    'CustDev', 'CJM', 'User Story Map', 'User Persona',
    'Agile', 'Scrum', 'Kanban', 'Jira', 'A/B-тесты',
    'Дизайн-тесты', 'Project Management', 'Amplitude'    
]