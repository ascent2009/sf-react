import React, { Component } from 'react';
import './style.sass'

class ArticleContent extends Component {
    
    render() {
        const {text, skills, portfolio} = this.props
        return (
            <div>
               {text} 
                <ul className="selectedItems" style={!skills ? {display: 'none'} : null}>
                    {skills ? skills.map((it, index) => (
                        <li className="selectedItem" key={index}>
                            {it}
                        </li>
                    )) : null
                    }
                </ul>
               <ul className="portfolioList" style={!portfolio ? {display: 'none'} : null}>
                    {portfolio ? portfolio.map((link, index) => (
                        <li className="portfolioURL" key={index}>
                            <a href={`https://${link}`} target="_blank" rel="noreferrer">{link}</a>
                        </li>
                    )) : null
                    }
            </ul>
            </div>
        );
    }
}

export default ArticleContent;