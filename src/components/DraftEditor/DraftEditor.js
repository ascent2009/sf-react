import React, { useState } from "react";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import { convertToHTML } from "draft-convert";
import DOMPurify from "dompurify";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./draft.scss";

const DraftEditor = (props) => {
  const {placeholder, value, key, name} = props;
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );

  // let _contentState = ContentState.createFromText("Sample content state");
  // const raw = convertToRaw(_contentState);
  // const [contentState, setContentState] = useState(raw); // ContentState JSON
  const [convertedContent, setConvertedContent] = useState(null);

  const handleEditorChange = (state) => {
    setEditorState(state);
    convertContentToHTML();
  };

  const convertContentToHTML = () => {
    let currentContentAsHTML = convertToHTML(editorState.getCurrentContent());
    setConvertedContent(currentContentAsHTML);
  };

  // const createMarkup = (html) => {
  //   return {
  //     __html: DOMPurify.sanitize(html),
  //   };
  // };

  return (
    <div className="general-wrapper">
      {/* <header style={{ textAlign: "center" }}>
        <h1>Rich Text Editor Example</h1>
      </header> */}
      <Editor
        defaultEditorState={editorState}
        placeholder={placeholder}
        // defaultContentState={contentState}
        // onEditorStateChange={setEditorState}
        // onContentStateChange={setContentState}
        onEditorStateChange={handleEditorChange}
        wrapperClassName="wrapper-class"
        editorClassName="editor-class"
        toolbarClassName="toolbar-class"
        toolbar={{
          inline: {
            inDropdown: false,
            options: ["bold", "italic", "underline"],
          },
          list: {
            inDropdown: false,
            options: ["unordered"],
          },
          blockType: {
            inDropdown: false,
            options: [],
          },
          fontSize: {
            inDropdown: false,
            options: [],
          },
          fontFamily: {
            inDropdown: false,
            options: [],
          },
          textAlign: { inDropdown: false, options: [] },
          colorPicker: {
            colors: [],
          },
          emoji: { emojis: [] },
          link: { inDropdown: false, options: [] },
          history: { inDropdown: false },
        }}
      />
      {/* <div
        className="preview"
        dangerouslySetInnerHTML={createMarkup(convertedContent)}
      ></div> */}
    </div>
  );
};

export default DraftEditor;
