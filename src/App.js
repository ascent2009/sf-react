import "./App.css";
// import GeneralInfoPage from './components/GeneralInfoPage';
// import StudentProfilePage from './components/StudentProfilePage';
import AboutMeBlock from "./components/AboutMeBlock";
// import DraftEditor from "./components/DraftEditor/DraftEditor";

function App() {
  return (
    <div>
      {/* <GeneralInfoPage /> */}
      {/* <StudentProfilePage /> */}
      <AboutMeBlock />
      {/* <DraftEditor /> */}
    </div>
  );
}

export default App;
